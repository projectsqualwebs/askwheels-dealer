import axios from 'axios';
import Auth from '../asyncStore/Index';
import {
  U_LOGIN,
  AS_USER_TOKEN,
  U_VEHICLE_MAKE,
  U_GET_VEHICLE_VARIENT,
  U_GET_VEHICLE_MODEL,
  U_ADD_VEHICLE,
  U_GET_LISTED_VEHICLE,
  U_GET_BID_VEHICLE,
  U_GET_VEHICLE_COLOR,
  U_DEALER_STATISTICS,
} from '../res/Constants';
import {Alert} from 'react-native';
import Toast from 'react-native-simple-toast';

const baseURL = 'http://54.176.179.158:1337/api/v1/';

const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);
  await console.log('user token is:-', token);
  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }
  return headers;
};

const request = async (method, path, body) => {
  const url = `${baseURL}${path}`;
  const options = {method, url, headers: await headers()};

  if (body) {
    options.data = body;
  }

  console.log('options are:-', options, body);
  return axios(options);
};

export default class API {
  loginAPI(data) {
    return request('POST', U_LOGIN, data);
  }

  getMakeData() {
    return request('GET', U_VEHICLE_MAKE);
  }

  getVarientData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_VARIENT + val);
    }
  }

  getColorData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_COLOR + val);
    }
  }

  getListedVehicleData() {
    return request('GET', U_GET_LISTED_VEHICLE);
  }

  getVehicleBasedonType(val) {
    if (val) {
      return request('GET', U_GET_LISTED_VEHICLE + '?type=' + val);
    }
  }

  getBidVehicleData() {
    return request('GET', U_GET_BID_VEHICLE);
  }

  getStatisticData() {
    return request('GET', U_DEALER_STATISTICS);
  }

  getModelData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_MODEL + val);
    }
  }

  saveInspectionData(val) {
    return request('POST', U_ADD_VEHICLE, val);
  }
}
