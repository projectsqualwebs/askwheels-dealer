'use strict';

import {StyleSheet} from 'react-native';
import COLOR from './Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FONTS from './Fonts';

export default StyleSheet.create({
  //UIView
  container: {
    flex: 1,
    backgroundColor: COLOR.WHITE,
  },
  shadow_view: {
    shadowColor: COLOR.GRAY,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    elevation: 3,
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
  },

  //UILabel
  heading_label: {
    fontSize: FONTS.LARGE,
    fontFamily: FONTS.FAMILY_MEDIUM,
    color: COLOR.BLACK,
  },
  subheading_label: {
    fontSize: FONTS.MEDIUM,
    fontFamily: FONTS.FAMILY_BOLD,
    color: COLOR.BLACK,
  },
  medium_label: {
    fontSize: FONTS.REGULAR,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    color: COLOR.BLACK,
  },
  body_label: {
    fontSize: FONTS.REGULAR,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.BLACK,
  },
  small_label: {
    fontSize: FONTS.SMALL,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.BLACK,
  },

  //UIText field
  text_input_text: {
    color: COLOR.BLACK,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
  },
  text_input_heading: {
    fontSize: FONTS.SMALL,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.VOILET,
  },

  //UIBUttons
  button_font: {
    color: COLOR.WHITE,
    fontSize: FONTS.REGULAR,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    alignSelf: 'center',
  },
  button_content: {
    height: 55,
    width: wp(90),
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  //UIImage
  backgroundImage: {
    flex: 1,
    width: wp(100),
    height: hp(100),
  },

  back_arrow: {
    height: 45,
    width: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },

  line_view: {
    borderBottomColor: '#e9e9e9',
    borderBottomWidth: 1,
    width: wp(90),
    alignSelf: 'center',
  },
});
