import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Styles from '../res/styles/Styles';

export const CustomButton = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.button_content, {backgroundColor: props.bg}]}>
      <Text style={[Styles.button_font, {color: props.labelColor}]}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button_content: {
    height: 55,
    width: wp(90),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
