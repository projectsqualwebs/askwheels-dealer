import React from 'react';
import {
  View,
  StyleSheet,
  Modal,
  Alert,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';

import IMAGES from '../res/styles/Images';
import Styles from '../res/styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../res/styles/Color';
import FONTS from '../res/styles/Fonts';
import {FloatingTitleTextInputField} from '../commonView/FloatingTextField';
import {CustomButton} from '../commonView/CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
//import Gallery from 'react-native-image-gallery';
import GallerySwiper from 'react-native-gallery-swiper';
import YourGalleryComponent from './YourGalleryComponent';

export default (GalleryView = props => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.handleGalleryView ();
      }}
    >
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          {/* <TouchableOpacity 
            style={{
             alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}
            onPress={() => {
              props.handleGalleryView ();
            }}
          >
           <GallerySwiper
                style={{
                  backgroundColor: '#ffffff',
                  flex: 1,
                  width: wp (100),
                  zIndex: 101,
                }}
                initialPage={0}
                resizeMode={'contain'}
                pageMargin={10}
                sensitiveScroll={false}
                enableScale={true}
                removeClippedSubviews={true}
                images={props.data.map (val => {
                  console.log ('val is:-', val);
                  return {
                    source: {uri: val},
                    dimensions: {
                      width: wp (100),
                      height: heightPercentageToDP (50),
                    },
                  };
                })}
              />
            <View
              style={{
                width: wp (100),
                height: heightPercentageToDP (50),
                backgroundColor: COLOR.WHITE,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                zIndex: 100,
              }}
            />
          </TouchableOpacity>*/}
          <YourGalleryComponent data={props.data} />
        </View>
      </View>
    </Modal>
  );
});

const styles = StyleSheet.create ({
  modal: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    backgroundColor: '#000000cc',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
});

//  handleGalleryView() {
//     this.setState({isGalleryViewVisible: false});
//   }

//   <GalleryView
//           data={this.state.inspectionData.files}
//           handleGalleryView={this.handleGalleryView}
//           modalVisible={this.state.isGalleryViewVisible}
//         />
