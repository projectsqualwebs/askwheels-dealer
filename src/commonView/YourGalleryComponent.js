import Gallery from 'react-native-photo-gallery';
import React from 'react';

export default class YourGalleryComponent extends React.Component {
  constructor (props) {
    super (props);
  }

  render () {
    const data = [];
    for (var i = 0; i < this.props.data.length; i++) {
      let image = {
        id: i.toString (),
        image: {uri: this.props.data[i]},
      };
      data.push (image);
    }

    return <Gallery data={data} />;
  }
}
