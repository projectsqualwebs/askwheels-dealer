import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  userDetail: null,
  makeData: [],
  modelData: [],
  varientData: [],
  inspectionDetail: null,
  listedVehicleData: [],
  vehicleDataBasedonType:[],
  bidVehicleData: [],
  colorData: [],
  statisticData: null,
  showIndicator:false,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.USER_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userDetail: action.data,
        });
      }
      return newState;
    case types.MAKE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          makeData: action.data,
        });
      }
      return newState;
    case types.VARIENT_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          varientData: action.data,
        });
      }
      return newState;
    case types.MODEL_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          modelData: action.data,
        });
      }
      return newState;
    case types.VEHICLE_INSPECTION_DATA:
      newState = cloneObject(state);

      if (action.data) {
        newState = Object.assign({}, newState, {
          inspectionDetail: action.data,
        });
      }
      return newState;
    case types.REMOVE_VEHICLE_INSPECTION_DATA:
      newState = cloneObject(state);

      if (action.data) {
        newState = Object.assign({}, newState, {
          inspectionDetail: null,
        });
      }
      return newState;
    case types.LISTED_VEHICLE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          listedVehicleData: action.data,
        });
      }
      return newState;
    case types.VEHICLE_ON_BID:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          bidVehicleData: action.data,
        });
      }
      return newState;
    case types.COLOR_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          colorData: action.data,
        });
      }
      return newState;
    case types.VEHICLE_STATISTICS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          statisticData: action.data,
        });
      }
      return newState;
    case types.VEHICLE_DATA_BASEDON_TYPE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          vehicleDataBasedonType: action.data,
        });
      }
      return newState;
    case types.SHOW_INDICATOR:
      Alert.alert('indei')
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          showIndicator: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        userDetail: null,
        makeData: [],
        modelData: [],
        varientData: [],
        inspectionDetail: null,
        listedVehicleData: [],
      });

      return newState;
    default:
      return state || newState;
  }
}
