import React from 'react';
import {View} from 'react-native';
import {AS_INITIAL_ROUTE} from '../../res/Constants';

import {CommonActions} from '@react-navigation/native';
import COLOR from '../../res/styles/Color';
import Auth from '../../asyncStore/Index';

const auth = new Auth();

export default class InitialLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRoute: 'SplashScreen',
    };
  }

  async componentDidMount() {
    const routeName = await auth.getValue(AS_INITIAL_ROUTE);

    if (routeName === null || routeName === '') {
      this.props.navigation.navigate(this.state.initialRoute);
    } else {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: routeName}],
        }),
      );
    }
  }

  // Render any loading content that you like here
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          backgroundColor: COLOR.BLACK,
        }}>
        {/* <ImageBackground style={{ width: widthPercentageToDP('100%'), height: heightPercentageToDP('100%') }} source={IMAGES.gradient_icon} >
				</ImageBackground> */}
      </View>
    );
  }
}
