import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Modal,
  ImageBackground,
} from 'react-native';

import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTextField';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {CustomButton} from '../../commonView/CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';

export default LoginScreen = (props) => {
  const navigation = useNavigation();
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.handleGetin(1);
      }}>
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          <KeyboardAwareScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            bounces={false}>
            <ImageBackground
              style={Styles.backgroundImage}
              resizeMode={'cover'}
              source={IMAGES.bg}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: wp(90),
                  alignSelf: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={IMAGES.appicon}
                  resizeMode={'contain'}
                  style={styles.appicon_image}
                />
                <TouchableOpacity
                  onPress={() => props.handleGetin(1)}
                  style={[Styles.back_arrow, {borderRadius: 22.5}]}>
                  <Image
                    style={{height: 40, width: 40, borderRadius: 20}}
                    source={IMAGES.left_arrow_white}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  marginVertical: 30,
                }}>
                <Text
                  style={[
                    Styles.heading_label,
                    {color: COLOR.WHITE, marginBottom: 10},
                  ]}>
                  Welcome to Ask Wheels
                </Text>
                <Text style={[Styles.medium_label, {color: COLOR.WHITE}]}>
                  Please enter your account credentials{'\n'}to login to Ask
                  Wheels.
                </Text>
              </View>
              <View style={styles.white_view}>
                <View style={styles.textfield_view}>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="email"
                      title="Email Address"
                      keyboardType={'email-address'}
                      value={props.email}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View style={[styles.textfield_blueview]}>
                    <FloatingTitleTextInputField
                      attrName="password"
                      title="Password"
                      isSecureTextEntry={true}
                      value={props.password}
                      placeholderTextColor={COLOR.RED}
                      updateMasterState={props.handleState}
                    />
                  </View>
                </View>
                <CustomButton
                  text={'Get in'}
                  bg={COLOR.LIGHT_BLUE}
                  onPress={props.handleGetin}
                  labelColor={COLOR.WHITE}
                />
                <View style={[styles.forgot_pass_view]}>
                  <Text style={Styles.small_label}>Forgot your password</Text>
                  <TouchableOpacity>
                    <Text
                     onPress={() => {navigation.navigate('ForgetPassword');
                    props.handleGetin(1);
                    }}
                      style={[Styles.body_label, {color: COLOR.LIGHT_BLUE}]}>
                      {' '}
                      Click here
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ImageBackground>
          </KeyboardAwareScrollView>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(100),
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 0,
    position: 'absolute',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textfield_view: {
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 50,
  },
  textfield_blueview: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.BLUE_SHADOW,
    borderRadius: 10,
    height: 60,
    paddingTop: 5,
  },
  forgot_pass_view: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 30,
    alignSelf: 'center',
  },
});
