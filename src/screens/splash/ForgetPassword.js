import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import COLOR from '../../res/styles/Color';
import Styles from '../../res/styles/Styles';
import {Text, View, Image} from 'react-native';
import IMAGES from '../../res/styles/Images';
import {CustomButton} from '../../commonView/CustomButton';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import YourGalleryComponent from '../../commonView/YourGalleryComponent';

export default (ForgetPassword = props => {
  return (
    <View style={[{flex: 1, backgroundColor: COLOR.LIGHT_BLUE}]}>
      <TouchableOpacity
        style={[Styles.back_arrow, {borderRadius: 22.5, marginTop: 5}]}
      >
        <Image
          style={{height: 24, width: 24, borderRadius: 20}}
          source={IMAGES.left_arrow_white}
        />
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          width: wp (90),
          marginVertical: 15,
        }}
      >
        <Text style={[Styles.heading_label, {color: COLOR.WHITE}]}>
          Forget Password
        </Text>
      </View>
      <Text style={[{marginStart: 20, color: COLOR.WHITE}]}>
        Please Enter Registered Email Address
      </Text>
      <View
        style={[
          {
            margin: 20,
            backgroundColor: COLOR.WHITE,
            borderRadius: 5,
            paddingStart: 5,
          },
        ]}
      >
        <TextInput
          attrName="email"
          title="Email Address"
          keyboardType={'email-address'}
          placeholder={'Email Address'}
        />
      </View>
      <View
        style={{
          marginBottom: 20,
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        }}
      >
        <CustomButton
          text={'Enter Email'}
          bg={COLOR.WHITE}
          onPress={props.handleGetin}
          labelColor={COLOR.LIGHT_BLUE}
        />
      </View>

    </View>
  );
  //<YourGalleryComponent />
});
