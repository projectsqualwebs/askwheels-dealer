import React from 'react';
import {View, StyleSheet} from 'react-native';
import {LineChart} from 'react-native-chart-kit';
import {widthPercentageToDP} from 'react-native-responsive-screen';

export default class BezireLineChart extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <LineChart
          bezier
          data={{
            labels: ['20', '21', '22', '23', '24', '25', '26'],
            datasets: [
              {
                data: ['10', '29', '70', '40', '50', '60', '70'],
                color: (opacity = 1) => `rgba(255, 108, 2, ${opacity})`,
              },
              {
                data: ['16', '20', '20', '10', '90', '40', '10'],
                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              },
            ],
          }}
          withDots={false}
          withShadow={false}
          withInnerLines={true}
          withOuterLines={false}
          withVerticalLines={false}
          width={widthPercentageToDP(90)}
          height={250}
          yAxisInterval={1}
          transparent={true}
          chartConfig={{
            decimalPlaces: 0,
            strokeWidth: 5,
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            // labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 0,
              alignSelf: 'center',
            },
          }}
          style={{
            alignSelf: 'center',

            marginLeft: -40,
          }}
          svg={{stroke: 'rgb(134, 65, 244)'}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
