import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import BezireLineChart from './LineChart';
import { connect } from 'react-redux';
import Actions from '../../../redux/actions/Actions';

class StatisticsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      dateType: 2,
      carData: [
        {
          id: 1,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 2,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 3,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 4,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 5,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 6,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 7,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 8,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 9,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 10,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
      ],
      vehicleTypes: [
        {
          id: 1,
          name: 'Cars Added',
          percent: '100',
        },
        {
          id: 2,
          name: 'Cars Sold',
          percent: '86',
        },
        {
          id: 3,
          name: 'On Bidding',
          percent: '10',
        },
        {
          id: 4,
          name: 'Direct Buy',
          percent: '04',
        },
      ],
      statisticData: null
    };
     
    this.handleDateType = this.handleDateType.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  componentDidMount() {
    this.props.getStatisticsData()
  }

  componentWillReceiveProps(props) {
    if (props.statisticData != this.props.statisticData) {
      this.setState({
        statisticData: props.statisticData
      })
    }
  }

  commonView = (props) => {
    return (
      <TouchableOpacity
        onPress={() => {
         // this.setState({selectedIndex: item.id});
        }}>
        <View
          style={[
            {
              borderRightColor: '#eaeaea',

              borderRightWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              width: wp(90) / 4,
              height: 110,
              alignSelf: 'center',
            },
          ]}>
          <View
            style={{
              height: 50,
              width: 50,
              borderRadius: 25,
              borderWidth: 3,
              borderColor: COLOR.BLACK,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[Styles.medium_label, {color: COLOR.LIGHT_BLUE}]}>
              {props.value}
            </Text>
          </View>
          <Text
            style={[
              Styles.small_label,
              {
                marginTop: 10,
                fontFamily: FONTS.FAMILY_MEDIUM,
                color: COLOR.BLACK,
              },
            ]}>
            {props.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {selectedIndex, dateType, statisticData} = this.state;
    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{width: 100}} />
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            Statistics
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}>
            {statisticData ? statisticData.totalVehicle : 0} Car(s)
          </Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>
          <View
            style={[
              styles.vehicle_type_view,
              {backgroundColor: COLOR.WHITE, paddingHorizontal: 15},
            ]}>
            <View
              style={{
                marginVertical: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View>
                <Text style={[Styles.body_label, {color: COLOR.GRAY}]}>
                  Today
                </Text>
                <Text style={[Styles.medium_label]}>20-11-20 to 26-11-20</Text>
              </View>

              <View
                style={[
                  Styles.back_arrow,
                  {backgroundColor: COLOR.LIGHT_BLUE, borderRadius: 10},
                ]}>
                <Image
                  style={{height: 30, width: 30, tintColor: COLOR.WHITE}}
                  source={IMAGES.calendar}
                />
              </View>
            </View>
            <View style={Styles.line_view} />
            <View
              style={{
                flexDirection: 'row',
              }}>
              <TouchableWithoutFeedback onPress={() => this.handleDateType(1)}>
                <View
                  style={{
                    flex: 1,
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor:
                      dateType == 1 ? COLOR.DARK_BLUE : COLOR.WHITE,
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: dateType == 1 ? COLOR.WHITE : COLOR.BLACK,
                        width: null,
                      },
                    ]}>
                    Today
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => this.handleDateType(2)}
                style={{
                  backgroundColor: COLOR.DARK_BLUE,
                  height: 50,
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    flex: 1,
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor:
                      dateType == 2 ? COLOR.DARK_BLUE : COLOR.WHITE,
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: dateType == 2 ? COLOR.WHITE : COLOR.BLACK,
                        width: null,
                      },
                    ]}>
                    Weekly
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.handleDateType(3)}>
                <View
                  style={{
                    flex: 1,
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor:
                      dateType == 3 ? COLOR.DARK_BLUE : COLOR.WHITE,
                    width: null,
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: dateType == 3 ? COLOR.WHITE : COLOR.BLACK,
                        width: null,
                      },
                    ]}>
                    Monthly
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>

          <View
            style={[
              styles.vehicle_type_view,
              {marginTop: 0, flexDirection: 'row'},
            ]}>
            <this.commonView
              name={'Cars Added'}
              value={statisticData ? statisticData.totalVehicle : 0}
            />
            <this.commonView
              name={'Cars Sold'}
              value={statisticData ? statisticData.sold : 0}
            />
            <this.commonView
              name={'Cars on Bid'}
              value={statisticData ? statisticData.onBid : 0}
            />
            <this.commonView
              name={'Direct Buy'}
              value={statisticData ? statisticData.directBuy : 0}
            />
            {/* <FlatList
              data={this.state.vehicleTypes}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({selectedIndex: item.id});
                  }}>
                  <View
                    style={[
                      {
                        borderRightColor: '#eaeaea',

                        borderRightWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: wp(90) / 4,
                        height: 110,
                        alignSelf: 'center',
                      },
                    ]}>
                    <View
                      style={{
                        height: 50,
                        width: 50,
                        borderRadius: 25,
                        borderWidth: 3,
                        borderColor: COLOR.BLACK,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={[
                          Styles.medium_label,
                          {color: COLOR.LIGHT_BLUE},
                        ]}>
                        {item.percent}
                      </Text>
                    </View>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          marginTop: 10,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                          color:
                            selectedIndex == item.id
                              ? COLOR.LIGHT_BLUE
                              : COLOR.BLACK,
                        },
                      ]}>
                      {item.name}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              horizontal={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            /> */}
          </View>
          <View
            style={{
              width: wp(90),

              alignSelf: 'center',
              padding: 15,
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              shadowOffset: {width: 0.2, height: 0.2},
              shadowOpacity: 0.2,
              shadowColor: COLOR.GRAY,
              elevation: 3,
              paddingBottom: 0,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 10,
              }}>
              <View>
                <Text style={Styles.medium_label}>
                  Total number of car registered and sold
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                  }}>
                  <Image
                    style={{
                      height: 10,
                      width: 10,
                      marginRight: 5,
                      tintColor: 'FF6C02',
                    }}
                    resizeMode={'contain'}
                    source={IMAGES.blue_dot}
                  />
                  <Text
                    style={[
                      Styles.body_label,
                      {color: COLOR.GRAY, marginRight: 20},
                    ]}>
                    Registered
                  </Text>
                  <Image
                    style={{
                      height: 10,
                      width: 10,
                      marginHorizontal: 5,
                      tintColor: COLOR.BLACK,
                    }}
                    resizeMode={'contain'}
                    source={IMAGES.blue_dot}
                  />
                  <Text style={[Styles.body_label, {color: COLOR.GRAY}]}>
                    Sold
                  </Text>
                </View>
              </View>

              <TouchableOpacity>
                <Image
                  style={{height: 40, width: 40, marginHorizontal: 5}}
                  source={IMAGES.refresh_icon}
                  resizeMode={'contain'}
                 />
              </TouchableOpacity>
            </View>
            {/* <BezireLineChart /> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 250,
    alignSelf: 'center',
    width: wp(90),
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  search_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    paddingHorizontal: 20,
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    marginBottom: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
     statisticData: state.reducer.statisticData
  }
}

const mapDispatchToProps = (Dispatch) => {
  return {
    getStatisticsData: (val) => {
      Dispatch(Actions.getStatisticData(val))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatisticsScreen);