import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Auth from '../../../asyncStore/Index';
import {
  AS_INITIAL_ROUTE,
  AS_USER_DETAIL,
  AS_USER_TOKEN,
} from '../../../res/Constants';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';
import Actions from '../../../redux/actions/Actions';
import {CommonActions} from '@react-navigation/native';

let auth = new Auth();

class SettingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleDateType = this.handleDateType.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  handleLogout = () => {
    let that = this;

    this.props.logoutUser();
    that.setState({isLoading: false});
    auth.remove(AS_USER_TOKEN);
    auth.remove(AS_USER_DETAIL);
    auth.remove(AS_INITIAL_ROUTE);
    Toast.show('Logged Out');
    that.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'SplashScreen'}],
      }),
    );
  };

  render() {
    const {selectedIndex, dateType} = this.state;
    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{width: 100}} />
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            My Account
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}></Text>
        </View>
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: COLOR.BLUE_BG,
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <View
            style={[
              Styles.shadow_view,
              {paddingHorizontal: 15, width: wp(90), marginTop: 20},
            ]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyCars')}>
              <Text style={[Styles.body_label, {height: 40, marginTop: 15}]}>
                My Cars
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
            //  onPress={() => this.props.navigation.navigate('AddNewListing')}
            >
              <Text style={[Styles.body_label, {height: 40}]}>
                Account Settings
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ManageAccounts')}>
              <Text style={[Styles.body_label, {height: 40}]}>
                Manage Accounts
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={[
              Styles.shadow_view,
              {paddingHorizontal: 15, width: wp(90), marginTop: 15},
            ]}>
            <TouchableOpacity>
              <Text style={[Styles.body_label, {height: 40, marginTop: 15}]}>
                Terms & conditions
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={[Styles.body_label, {height: 40}]}>
                Privacy policy
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={[Styles.body_label, {height: 40}]}>Share URL</Text>
            </TouchableOpacity>
          </View>
          <View
            style={[
              Styles.shadow_view,
              {paddingHorizontal: 15, width: wp(90), marginTop: 50},
            ]}>
            <TouchableOpacity
              onPress={() => {
                this.handleLogout();
              }}>
              <Text style={[Styles.body_label, {height: 40, marginTop: 15}]}>
                Log Out
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 250,
    alignSelf: 'center',
    width: wp(90),
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  search_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    paddingHorizontal: 20,
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    marginBottom: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => {
      dispatch(Actions.logoutUser());
    },
  };
};

export default connect(null, mapDispatchToProps)(SettingScreen);
