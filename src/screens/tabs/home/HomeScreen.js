import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Alert,
} from 'react-native';

import {ScrollView} from 'react-native-gesture-handler';
import database from '@react-native-firebase/database';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {DotView} from '../../../commonView/BackView';
import Actions from '../../../redux/actions/Actions';
import {connect} from 'react-redux';
import {insertComma, getRemainingTime} from '../../../commonView/Helpers';
import Indicator from '../../../commonView/ActivityIndicator';
import CountDown from 'react-native-countdown-component';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      carData: [],
      vehicleData: [],
    };
    this.props.getListedVehicles();
    this.props.getVehicleOnBid();
    this.handleAnimationEnd = this.handleAnimationEnd.bind(this);
  }

  componentDidMount() {
    this.handleRealtimeDB();

    if (
      this.state.vehicleData.length == 0 &&
      this.props.listedVehicleData.length
    ) {
      this.setState({
        vehicleData: this.props.listedVehicleData,
      });
    }

    if (this.state.carData.length == 0 && this.props.bidVehicleData.length) {
      this.setState({
        carData: this.props.bidVehicleData,
      });
    }
  }

  componentWillReceiveProps(props) {
    if (props.listedVehicleData != this.props.listedVehicleData) {
      this.setState({vehicleData: props.listedVehicleData, isLoading: false});
    }

    if (props.bidVehicleData != this.props.bidVehicleData) {
      this.setState({carData: props.bidVehicleData, isLoading: false});
    }
  }

  handleRealtimeDB = () => {
    
      database()
        .ref('highBid')
        .on('value', (snapshot) => {
          setTimeout(
            function () {
              let carData = this.state.carData;
              snapshot.forEach((childSnapshot, i) => {
                carData.forEach((val, index) => {
                  if (val.id == childSnapshot.key) {
                    carData[index].highBid = childSnapshot.val().amount;
                    carData[index].isAnimating = true;
                  }
                });

                this.setState({
                  carData: carData,
                });
              });
            }.bind(this),
            1000,
          );
        });

      database()
        .ref('closedBids')
        .on('value', (snapshot) => {
          let carData = this.state.carData;
          snapshot.forEach((childSnapshot, i) => {
            carData.forEach((val, index) => {
              if (val.id == childSnapshot.key) {
                carData[index].closeForBid = childSnapshot.val().closeForBid;
                carData[index].isAnimating = true;
                this.props.getVehicleOnBid(1);
              }
            });
            this.setState({
              carData: carData,
            });
          });
        });

      database()
        .ref('vehicleBidTime')
        .on('value', (snapshot) => {
          let carData = this.state.carData;
          snapshot.forEach((childSnapshot, i) => {
            carData.forEach((val, index) => {
              if (val.id == childSnapshot.key) {
                carData[index].bidCloseTime = childSnapshot.val().bidCloseTime;
              }
            });
            this.setState({
              carData: carData,
            });
          });
        });

      database()
        .ref('bidVehicles')
        .on('value', (snapshot) => {
          if (this.state.handlerSet == false) {
            this.setState({
              handlerSet: true,
            });
          } else {
            this.props.getVehicleOnBid(1);
          }
        });
    
  };

  handleAnimationEnd = (index) => {
    let data = this.state.carData;
    data[index].isAnimating = false;
    this.setState({
      carData: data,
    });
  };

  getCommisionPrice = (percent,value) => {
  percent = percent/100;
  value = value*percent;
  return value;
 } 

  render() {
    let {isLoading, carData, vehicleData} = this.state;
    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <View style={styles.blue_view}>
          <Image source={IMAGES.appicon} style={styles.image_logo} />
          <View style={styles.search_view}>
            <TextInput
              style={Styles.body_label}
              placeholder={'Search by name'}
              placeholderTextColor={'#696969'}
            />
            <Image
              style={{height: 20, width: 20}}
              resizeMode={'contain'}
              source={IMAGES.search}
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              this.props.getInspectionDetail(null, 3);
              this.props.navigation.navigate('VehicleDetail');
            }}
            style={{
              height: 40,
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 20,
            }}>
            <Text
              style={[
                Styles.body_label,
                {
                  textDecorationLine: 'underline',
                  textDecorationColor: COLOR.WHITE,
                  color: COLOR.WHITE,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                },
              ]}>
              + Add New Listing
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 15,
            width: wp(100),
            alignSelf: 'center',
            paddingHorizontal: 15,
          }}>
          <Text style={[Styles.medium_label]}>On Going Auctions</Text>
          {/* <TouchableOpacity
            onPress={() => this.props.navigation.navigate('VehicleList')}>
            <Text style={Styles.body_label}>View all</Text>
          </TouchableOpacity> */}
        </View>
        <FlatList
          style={[
            styles.flatlist_view,
            {alignSelf: 'center', flex: 1, paddingTop: 0},
          ]}
          data={this.state.carData}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('VehicleInspection', {
                  data: item,
                })
              }>
              <Animatable.View
                animation={item.isAnimating ? 'shake' : ''}
                iterationCount={1}
                duration={500}
                onAnimationEnd={() => this.handleAnimationEnd(index)}>
                <View
                  style={[
                    styles.car_on_bid,
                    {width: wp(90), marginBottom: 20},
                  ]}>
                  {item.closeForBid == false && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: wp(90),
                        alignSelf: 'center',
                        paddingHorizontal: 10,
                      }}>
                      <View
                        style={{
                          borderRadius: 10,
                          flex: 1,
                          paddingHorizontal: 0,
                          alignItems: 'flex-start',
                        }}>
                        <Text
                          style={[
                            Styles.body_label,
                            {
                              color: COLOR.BLACK,
                              fontFamily: FONTS.FAMILY_SEMIBOLD,
                              textAlign: 'left',
                            },
                          ]}>
                          Highest Bid: ₹
                          {item.highBid
                            ? insertComma(
                                item.highBid -parseFloat(this.getCommisionPrice(item.commissionPercent,item.highBid)).toFixed(2)
                              )
                            : insertComma(item.expectedPrice)}
                        </Text>
                        {(item.highBid != null ||
                          item.closeForBid == false) && (
                          <Text
                            style={[
                              Styles.small_label,
                              {
                                color: COLOR.GREEN,
                                textAlign: 'left',
                                marginVertical: 5,
                                fontFamily: FONTS.FAMILY_SEMIBOLD,
                              },
                            ]}>
                            Scheduled for Bidding
                          </Text>
                        )}
                      </View>
                      <View
                        style={{
                          borderRadius: 10,
                          flexDirection: 'row',
                          paddingHorizontal: 0,
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{
                            height: 22,
                            width: 22,
                            marginRight: 5,
                            tintColor: COLOR.BLACK,
                          }}
                          source={IMAGES.clock}
                          resizeMode={'contain'}
                        />

                        <CountDown
                          style={{
                            marginRight: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                          until={
                            item.closeForBid
                              ? 0
                              : getRemainingTime(item.bidCloseTime)
                          }
                          digitStyle={{
                            backgroundColor: 'transparent',
                            width: 20,
                          }}
                          digitTxtStyle={{
                            color: COLOR.BLACK,
                            width: 20,
                          }}
                          timeLabels={{m: null, s: null}}
                          timeToShow={['H', 'M', 'S']}
                          size={15}
                          separatorStyle={{
                            color: COLOR.BLACK,
                            marginBottom: 5,
                          }}
                          onFinish={() => {
                            let data = carData;
                            data[index].closeForBid = true;
                            this.setState({
                              vehicleData: data,
                            });
                          }}
                          showSeparator={true}
                        />
                        <Text style={{color: COLOR.LIGHT_TEXT, fontSize: 12}}>
                          left
                        </Text>
                      </View>
                    </View>
                  )}
                  <View
                    style={{
                      height: 250,
                      alignSelf: 'center',
                      width: wp(90),
                      marginTop: 0,
                    }}>
                    <FastImage
                      style={{
                        flex: 1,
                        height: 250,
                        overflow: 'hidden',
                      }}
                      resizeMode={FastImage.resizeMode.cover}
                      source={
                        item.files.length > 0
                          ? {
                              uri: item.files[0],
                              priority: FastImage.priority.normal,
                            }
                          : IMAGES.white_car
                      }>
                      <View
                        style={{
                          bottom: 0,
                          position: 'absolute',
                          backgroundColor: COLOR.BLACK_GRAD,
                          width: wp(90),
                          paddingHorizontal: 10,
                          justifyContent: 'center',
                          alignSelf: 'center',
                        }}>
                        <Text
                          style={[
                            Styles.subheading_label,
                            {
                              fontFamily: FONTS.FAMILY_MEDIUM,
                              color: COLOR.WHITE,
                            },
                          ]}>
                          {item.selectedMake
                            ? item.selectedMake.label
                            : item.make}{' '}
                          {item.selectedModel
                            ? item.selectedModel.label
                            : item.model}
                          (
                          {item.selectedVarient
                            ? item.selectedVarient.label
                            : item.variant}
                          )
                        </Text>
                      </View>
                    </FastImage>
                  </View>
                  <Text
                    style={[
                      {
                        width: wp(90),
                        alignSelf: 'center',
                        paddingHorizontal: 15,
                        marginTop: 10,
                        fontSize: FONTS.SMALL,
                        fontFamily: FONTS.FAMILY_REGULAR,
                      },
                    ]}>
                    <DotView />
                    {insertComma(item.odometerReading)}kms{'  '}
                    <DotView />
                    {item.transmission}
                    {'  '}
                    <DotView />
                    {item.numberOfOwners == 1
                      ? `${item.numberOfOwners}st`
                      : item.numberOfOwners == 2
                      ? `${item.numberOfOwners}nd`
                      : item.numberOfOwners == 3
                      ? `${item.numberOfOwners}rd`
                      : `${item.numberOfOwners}th`}{' '}
                    owner{'  '}
                    <DotView />
                    {item.manufacturingYear}
                    {'  \n'}
                    <DotView />
                    {item.rtoState ? item.rtoState : ''}
                    {'  '}
                    <DotView />
                    {item.pickupAddress ? item.pickupAddress : ''}
                  </Text>
                </View>
              </Animatable.View>
            </TouchableOpacity>
          )}
          numColumns={1}
          ListHeaderComponent={() =>
            !this.state.carData.length ? (
              <Text style={{textAlign: 'center', color: COLOR.LIGHT_TEXT}}>
                No Vehicle in Auction
              </Text>
            ) : null
          }
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />

        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            marginVertical: 20,
            width: wp(100),
            alignSelf: 'center',
          }}>
          <Text style={Styles.medium_label}>My Vehicles</Text>
          {/* <TouchableOpacity
            onPress={() => this.props.navigation.navigate('VehicleList')}>
            <Text style={Styles.body_label}>View all</Text>
          </TouchableOpacity> */}
        </View>

        <FlatList
          style={[styles.flatlist_view, {alignSelf: 'center', flex: 1}]}
          data={this.state.vehicleData}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('VehicleInspection', {
                  data: item,
                })
              }
              style={[styles.car_view]}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                }}>
                <FastImage
                  source={
                    item.files.length > 0
                      ? {
                          uri: item.files[0],
                          priority: FastImage.priority.normal,
                        }
                      : IMAGES.white_car
                  }
                  resizeMode={FastImage.resizeMode.cover}
                  style={{
                    width: 110,
                    marginRight: 10,
                    height: 80,
                    borderRadius: 10,
                  }}
                />
                <View style={{flex: 1, marginVertical: 0, marginHorizontal: 0}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'flex-start',
                      flex: 1,
                    }}>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        {item.selectedMake
                          ? item.selectedMake.label
                          : item.make}{' '}
                        {item.selectedModel
                          ? item.selectedModel.label
                          : item.model}
                      </Text>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            width: null,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        ₹ {insertComma(String(item.expectedPrice))}
                      </Text>
                    </View>
                    <AnimatedCircularProgress
                      style={{
                        backgroundColor: COLOR.WHITE,
                        marginLeft: 10,
                        marginRight: 0,
                      }}
                      size={35}
                      width={3}
                      fill={parseInt(item.avgRating ? item.avgRating : 0)}
                      rotation={0}
                      minva
                      tintColor={COLOR.LIGHT_BLUE}
                      backgroundColor={COLOR.LIGHT_GRAY}>
                      {(fill) => (
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              color: COLOR.LIGHT_BLUE,
                              fontFamily: FONTS.FAMILY_SEMIBOLD,
                            },
                          ]}>
                          {parseFloat(item.avgRating ? item.avgRating/10 : 0).toFixed(1)}
                        </Text>
                      )}
                    </AnimatedCircularProgress>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 0,
                      marginHorizontal: 0,
                    }}>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      {insertComma(String(item.odometerReading))} Kms |{' '}
                      {item.fuelType} |{' '}
                      {item.numberOfOwners == 1
                        ? `${item.numberOfOwners}st`
                        : item.numberOfOwners == 2
                        ? `${item.numberOfOwners}nd`
                        : item.numberOfOwners == 3
                        ? `${item.numberOfOwners}rd`
                        : `${item.numberOfOwners}th`}{' '}
                      owner
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
          numColumns={1}
          ListHeaderComponent={() =>
            !this.state.vehicleData.length ? (
              <Text
                style={{
                  textAlign: 'center',
                  color: COLOR.LIGHT_TEXT,
                  marginTop: 50,
                }}>
                No Vehicle(s) Found
              </Text>
            ) : null
          }
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
        {/* <Indicator loading={isLoading} /> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  blue_view: {
    width: wp(100),
    height: 220,
    backgroundColor: COLOR.LIGHT_BLUE,
  },
  image_logo: {
    height: 55,
    aspectRatio: 1,
    alignSelf: 'center',
    marginTop: 35,
  },
  search_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    paddingHorizontal: 20,
    width: wp(90),
    alignSelf: 'center',
    height: 45,
    marginTop: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  car_type_view: {
    width: wp(90),
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    marginTop: -40,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 1, height: 1},
  },
  hatchback: {
    borderRightColor: '#eaeaea',
    borderBottomColor: '#eaeaea',
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  sedan: {
    borderRightColor: '#eaeaea',
    borderBottomColor: '#eaeaea',
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  coupe: {
    borderRightColor: '#eaeaea',
    borderBottomColor: '#eaeaea',
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  minivan: {
    borderRightColor: '#eaeaea',
    borderRightWidth: 1,
  },
  suv: {
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 15,
    width: wp(100),
    alignSelf: 'center',
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    listedVehicleData: state.reducer.listedVehicleData,
    bidVehicleData: state.reducer.bidVehicleData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListedVehicles: (val) => {
      dispatch(Actions.getListedVehicles(val));
    },
    getInspectionDetail: (data, val) => {
      dispatch(Actions.inspectionDetail(data, val));
    },
    getVehicleOnBid: (type) => {
      dispatch(Actions.getVehicleOnBid(type));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
